const chkDrawFrames = document.querySelector('#DrawFrames');

  let drawFrames = chkDrawFrames.checked;
  chkDrawFrames.addEventListener('change', e=> { drawFrames = chkDrawFrames.checked;
  console.log('drawFrames', drawFrames);
  });

async function getInputData() {
  const data = await fetch('/api/trainingdata').then(d => d.json());
  // console.log(data);
  const divImg = document.getElementById('Img');
  if (drawFrames) {
    divImg.innerHTML = data.imgData.map(r => r.map(c => `<div style="background-color: rgb(${c},${c}, ${c})"></div>`).join('')).join('');
    document.querySelector('#Img+legend').innerText = data.label;
  }
  return {expectedValue: data.label, inputs: [].concat(...data.imgData)};
}

async function getAI(name) {
    //const data = await fetch('/api/AI').then(d => d.text());
    //console.log(data, import('/api/AI'));
    const AI = await import(`/api/AI/${name}/js`);
    console.log(AI.default);
    return new AI.default();
  }
  async function getAIs() {
    const AINames = await fetch('/api/AI');
    // console.log(AINames.json());
    return AINames.json();
  }
    
async function setupOutputs() {

console.log('setupOutputs');

    const tblOutputs = document.querySelector('#Outputs>tbody');

    tblOutputs.innerHTML = '';
    let outputs = 
    [0,1,2,3,4,5,6,7,8,9].map(i=>{
        let tr = document.createElement('tr');
        let tdImg = document.createElement('td');
        let divInner = document.createElement('div');
        let tdValue = document.createElement('td');
        let tdTargetValue = document.createElement('label');
        let tdScore = document.createElement('td');
        
        tdImg.appendChild(divInner);
        tr.appendChild(tdImg);
        tr.appendChild(tdValue);
        tr.appendChild(tdTargetValue);
        tr.appendChild(tdScore);
        tblOutputs.appendChild(tr);
        return (v, expectedResult)=> {
          let score = -3.0 * Math.abs(expectedResult - v); //high difference = very negative score = bad;
          if (drawFrames) { 
            tdValue.innerText = v;
            tdTargetValue.innerText = expectedResult;
            tdScore.innerText = score
            divInner.style.backgroundColor = `rgb(${v*256},${v*256}, ${v*256}`;
          }
          return score;
        };
    });
    const tdElapsedTime = document.querySelector('#ElapsedTime>td');
    const tdFrameScore = document.querySelector('#FrameScore>td');
    const tdAvgScore = document.querySelector('#AvgScore>td');
    const tdFPS = document.querySelector('#FPS>td');
    const tdName = document.querySelector('#Name>td');
    const spnProgress = document.querySelector('#Progress>td>div>span');
    const divProgress = document.querySelector('#Progress>td>div>div');
    const allScores=[];
    let totalScore=0;
    const timePenaltyMutliplier = 0.1 // score is deducted this much for every ms the frame takes
    return (outputData, timeElapsed, totalTimeElapsed, expectedResult, frameNumber, totalFrames, name) => {
      let frameScore = (-timeElapsed * timePenaltyMutliplier) + outputs.map((o,i)=>o(outputData[i], i==expectedResult?1:0)).reduce(sum);
      allScores.push(frameScore);
      totalScore+=frameScore;    
      if (drawFrames || frameNumber % 10 === 0) {
        // console.log('te', timeElapsed);
        tdName.innerText = name;
        tdElapsedTime.innerText = timeElapsed;
        tdFrameScore.innerText = frameScore;
        tdAvgScore.innerText = totalScore / frameNumber;
        tdFPS.innerText = (frameNumber / (totalTimeElapsed/1000)).toFixed(2);
        spnProgress.innerText = frameNumber  + ' / ' + totalFrames;
        divProgress.style.width = (frameNumber / totalFrames)*100  + '%';
      }
        return frameScore;
    };timePenaltyMutliplier
  }
  function sigmoid(value)
  {
    return 1 / (1 + (1.1 ** -value))
  }
  
  const divResults = document.querySelector('#Results');
  function saveScore(score){
    const div = document.createElement('div');
    const hue = sigmoid(score) * -240;
    div.style.backgroundColor = `hsl(${hue}deg, 100%, 50%)`;
    divResults.appendChild(div);
    div.setAttribute('title', score);
    div.innerText = score;
  }
function sum(acc=0,curr)
{
    return acc+curr;
}
(async () => {
  let aiNames = await getAIs();
  const pSetOutputs = setupOutputs();

  for (name of aiNames){
    const ai = await getAI(name);
    //const data = await getInputData();
    const start = new Date();
    //  const outputData = await ai.processFrame(data.inputs);
    const setOutputs = await pSetOutputs;
    let nextData =  getInputData();

    const totalFrames = 100;
    let allScores = 0;
    for (let frame=0;frame<totalFrames ;frame++)
    {
        let data = await nextData;
        nextData = getInputData();
        const frameStart = new Date();
        const outputData = await ai.processFrame(data.inputs);
     //   console.log(outputData);
        let elapsedTime=new Date() - frameStart;
        let totalElapsedTime=new Date() - start;
        let frameScore = setOutputs(outputData, elapsedTime, totalElapsedTime, data.expectedValue, frame, totalFrames, name);
        allScores += frameScore;
    }
    saveScore(allScores / totalFrames);
    ai.saveScore(allScores / totalFrames);
    }
})();