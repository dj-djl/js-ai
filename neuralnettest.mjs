// import * as babylon from 'babylon';
import fs from 'fs';
import JsonCycle from 'json-cycle';
import DigitsAI from './neuralnet/neuralnet';

(async () => {
  const inputCount = 28; // * 28;
  // const ai = new DigitsAI(28 * 28, 10);
  const ai = new DigitsAI(inputCount, 10);
  // console.log(ai.drawToSVG());
  // fs.writeFileSync('demo.json', JSON.stringify(ai, null, 2));
  fs.writeFileSync('demo.json', JsonCycle.stringify(ai, null, 2));
  fs.writeFileSync('demo.svg', ai.drawToSVG());

  fs.writeFileSync('demo.mjs', `${await ai.compile('B \\ob')}

(new Bob()).processFrame((new Array(${inputCount})).fill(0).map(()=>Math.random()));
`);
})().catch((ex) => {
  console.error(ex);
  process.exit(1);
});
