.phony: run lint format css

N=/usr/local/bin/n
LESS=node_modules/.bin/lessc

run: css
	$(N) use stable --experimental-modules index.mjs

lint:
	node_modules/.bin/eslint *.mjs web/*.js --color | less

demo: demo.mjs
	$(N) use stable --experimental-modules demo.mjs

demo.mjs: neuralnettest.mjs neuralnet/neuralnet.mjs neuralnet/mutableValues.mjs neuralnet/clamping.mjs
	$(N) use stable --experimental-modules neuralnettest.mjs
css: web/styles.css
%.css: %.less
	$(LESS) --math=strict $< $@