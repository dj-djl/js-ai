module.exports = {
  "extends": "airbnb-base",
  "rules": {
    "max-len": ["error", {
      "ignoreComments": true,
      "code": 120,
      "ignoreTemplateLiterals": true,
    }],
    "no-param-reassign": ["error", { "props": false}],
    "lines-between-class-members": "off",
    "no-multi-spaces": ["error", { ignoreEOLComments: true }],
    "no-else-return": "off",
    "no-console": "off",
    "no-restricted-syntax": [
      "error",
      {
        "selector": "ForInStatement",
        "message": "for..in loops iterate over the entire prototype chain, which is virtually never what you want. Use Object.{keys,values,entries}, and iterate over the resulting array."
      },
      //{
      //  "selector": "ForOfStatement",
      //  "message": "iterators/generators require regenerator-runtime, which is too heavyweight for this guide to allow them. Separately, loops should be avoided in favor of array iterations."
      //},
      {
        "selector": "LabeledStatement",
        "message": "Labels are a form of GOTO; using them makes code confusing and hard to maintain and understand."
      },
      {
        "selector": "WithStatement",
        "message": "`with` is disallowed in strict mode because it makes code impossible to predict and optimize."
      }
    ],
    quotes: ["error", "single", { "avoidEscape": true , "allowTemplateLiterals": true }],
    "no-loop-func": "off"
  }
};