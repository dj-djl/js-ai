import http from 'http';
import hjson from 'hjson';
import fs from 'mz/fs';
import path from 'path';
import childProcess from 'child_process';
import API from './api/API';

(async () => {
  const config = hjson.parse(await fs.readFile('config.hjson', 'utf8'));

  const staticFilesWhitelist = {
    '/': {
      path: 'web/index.html',
      contentType: 'text/html',
    }, '/favicon.ico': {
      path: 'web/favicon.ico',
      contentType: 'text/html',
    },
    '/gm.js': {
      path: 'web/gm/es.js',
      contentType: 'application/javascript',
    },
    '/index.js': {
      path: 'web/index.js',
      contentType: 'application/javascript',
    },
    '/styles.css': {
      path: 'web/styles.css',
      contentType: 'text/css',
    },
  };
  class HttpError extends Error {
    constructor(responseCode, content, contentType = 'text/plain') {
      super();
      this.responseCode = responseCode;
      this.content = content;
      this.contentType = contentType;
    }
  }


  function sendStaticFile(request, response, info) {
    // add stuff for checking cache etc.
    response.writeHead(200, {
      'content-type': info.contentType,
    });
    fs.createReadStream(info.path).pipe(response);
  // response.end();
  }

  const loadedAPIModules = [];
  function* loadAPIs(basePath) {
    // const loader = new NodeESModuleLoader();
    console.log('loading modules from ', basePath);
    for (const file of fs.readdirSync(basePath).map(f => path.join(basePath, f))) {
      // console.log('  ', file, fs.statSync(file).isDirectory());
      if (fs.statSync(file).isDirectory()) {
        yield* loadAPIs(file);
      } else if (fs.statSync(file).isFile()) {
        yield import(file)
          .then((res) => {
            // console.log('importing', file);
            if (!res.default) {
             // console.log('skipping module with no default export');
              return;
            }
            if (!(typeof res.default === 'function')) {
              // console.log('skipping non-function default export');
              return;
            }
            if (res.default === API)
            {
              // console.log('skip API class itself');
              return;
            }
            if (API.isPrototypeOf(res.default)) {
              // console.log('module\'s default export inherits from API class, adding to loaded modules');
              console.log(`Adding ${file} to loaded API modules`);
              loadedAPIModules.push(new res.default());
            }
          });
      } else {
        console.warn(`What is this thing you've asked me to scan?`, file);
      }
    }
  }
  await Promise.all(loadAPIs(path.join(path.dirname(process.argv[1]), 'api')));
  async function api(req, resp) {

    const requestUrl = new URL(req.url, "http://nowhere");
    // console.log(requestUrl);

    let apiPath= requestUrl.pathname.replace(/^\/api\//, '').split(/\//);
    for (const api of loadedAPIModules) {
      // console.log(api);
      let method = api.find(apiPath, req.method);
      if (method) {
        switch(method.responseType)
        {
          case 'json':
          {
            let response = await method.run();
            resp.writeHead(200, {"content-type": "application/json"});
            resp.write(JSON.stringify(response));
            resp.end();
            return;
          }
          case 'text':
          {
            let response = await method.run();
            resp.writeHead(200, {"content-type": method.contentType});
            resp.write(response);
            resp.end();
            return;
          }
          default:
            throw new HttpError(500, `responseType of ${module.responseType} for ${req.url} not supported`);    
        }
        return;
      } 
    }
    throw new HttpError(404, 'API not found');
  }
  http.createServer(async (req, resp) => {
    // console.log(req.url);
    try {
      if (req.url in staticFilesWhitelist) {
        return sendStaticFile(req, resp, staticFilesWhitelist[req.url]);
      } else if (req.url.startsWith('/api')) {
        return await api(req, resp);
      }
      console.error(req.url);
      throw new HttpError(404, 'Nothing at that URL');
    } catch (ex) {
      console.error(ex, ex instanceof HttpError);
      if (ex instanceof HttpError) {
        resp.writeHead(ex.responseCode, {
          'content-type': ex.contentType,
        });
        resp.write(ex.content);
        return resp.end();
      } else {
        resp.writeHead(500, {
          'content-type': 'text/plain',
        });
        resp.write(ex.toString());
        return resp.end();
      }
    }
  }).listen(config.httpPort, function listening() {
    const address = this.address();
    console.log('opened server on', address);
    const url = `http://[${address.address}]:${address.port}/`;
    console.log('url: ', url);

    if (config.browser && config.browser !== 'none') {
      childProcess.spawn(config.browser, [url]);
    }
  });
})().catch(ex => {
  console.error(ex);
  process.exit(1);
});
;
