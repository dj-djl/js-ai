import Neuralnet from './neuralnet';

class Generation extends Map {
  constructor(initialPopulationSize, generationNumber, setName) {
    super();
    for (let item = 0; item < initialPopulationSize; item += 1) {
      const itemName = `${setName}Gen${generationNumber}AI${item}`;
      this.set(itemName, new Neuralnet(28 * 28, 10));
    }
  }
  breed(newPopulationSize) {
    throw new Error('work out how to do this!');
  }
}
export default Generation;
