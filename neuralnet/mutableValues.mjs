const defaultMutability = 0.0001;
class MutableEnum {
  constructor(Enum) {
    const funcIndex = Math.random() * Object.keys(Enum).length;
    this.value = Enum[Object.keys(Enum)[parseInt(funcIndex, 10)]];
    this.mutability = Math.random() * defaultMutability;
  }
  valueOf() {
    return this.value;
  }
}
class MutableFloat {
  constructor(min, max) {
    if (typeof (max) === 'undefined') {
      this.value = min || 0;
    } else {
      this.value = min + (Math.random() * (max - min));
    }
    this.mutability = Math.random() * defaultMutability;
  }
  valueOf() {
    return this.value;
  }
}
export { MutableEnum, MutableFloat, defaultMutability };
