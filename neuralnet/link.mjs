import { MutableFloat, defaultMutability } from './mutableValues';

class Link {
  constructor(srcNeuron, targetNeuron) {
    this.srcNeuron = srcNeuron;
    // this.targetNeuron = targetNeuron; //causes a circular reference. TODO: uncomment this once serialisation is fixed.
    this.weight = new MutableFloat(-1, 1);
    this.weight.mutability = defaultMutability * 100; // Weights should be far more likely to change than anything else
    this.weightChangeAmount = new MutableFloat(0, defaultMutability); // but the amount by whcih they change should probably still be smallish
    this.chanceToDie = new MutableFloat(0, defaultMutability);
  }
  // toJSON()
  // {
  //     
  // }
}
export default Link;
