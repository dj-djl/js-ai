import Link from './link';
import { InputLayer, OutputLayer, VariableLayer } from './layers';

/*
Notes:
This is an evolutionary neural net - there is no back propagation here.
This is a deliberate design choice as the eventual target for the AI has no training data available.
* Chance:
    * all % chance parameters will themselves change, by tiny amounts, every generation
* Neural nets:
    * Layers:
        * % chance to die or split
            * When split there are 2 choices:
                1. Create identity links all with initial weight 0.5
                2. Create random links with random values
            * TODO: define behvaiour when dieing, especially if node count does not match...
    * Neuron:
        * Has a bias
            * bias can change
        * % chance to die or split
            *  When split there are 2 choices
                1. Copy node that is split, but half effective output weights after rebalancing
                2. Create random links with random weights both input and output
        * Choice of clamping functions
            * % chance for clamping function to change
            * list of parameters for clamping function
                * each parameter can change
        * % Chance to form a new link
    * Links:
        * has a weight
        * % chance for Weight to change
        * max % by which weight can change
        * % chance for max % change to change by fixed amount
        * % chance for link to sever
    * Inputs:
      * No dieing or splitting
    * Outputs;
      * Same as normal Neurons except:
        * No output links.
        * No dieing or splitting
    * Internal state:
      * Optional
      * Add additional outputs that also serve as inputs for the next frame. Initalised to 0
      * split/die chance the same as other Neurons
      * I'm not sure if this is wise or not!
* Evolution:
    *  Population size need not be constant
*/

class NeuralNet {
  constructor(inputCount, outputCount) {
    this.inputs = new InputLayer(inputCount);
    // lets create 2 layers of 16 neuronot s each by default
    // a really primitive brain probably doesn't have very many neurons,
    // so lets keep the node count low to begin with and leave room for it to evolve.
    // it also means early versions of the neural net should be quite quick. (hopefully)
    // it's also in line with 3blue1brown's tutorial, so it can't be bad... right?
    // https://www.youtube.com/watch?v=aircAruvnKk
    this.layers = [16, 16].map(nodeCount => new VariableLayer(nodeCount));
    this.outputs = new OutputLayer(outputCount);
    // add some random links
    let previousLayer = this.inputs;
    [...this.layers, this.outputs].forEach((layer) => {
      layer.neurons.forEach((neuron) => {
        const selectedNeurons = previousLayer.neurons.filter(() => Math.random() > 0.875); // select some neurons from the previous layer randomly
        neuron.links = selectedNeurons.map(src => new Link(src, neuron));
      });
      previousLayer = layer;
    });
  }
  drawToSVG() {
    const layerWidth = 1000 / (this.layers.length + 2);
    const previousLayerNodePositions = new Map();
    return `<?xml version="1.0" encoding="UTF-8" standalone="no"?>
    <svg xmlns="http://www.w3.org/2000/svg"
       width="1000px"
       height="1000px"
       viewBox="0 0 1000 1000"
       version="1.1">
       ${[this.inputs, ...this.layers, this.outputs].map((l, num) => l.drawToSVG(previousLayerNodePositions, layerWidth, 1000, num * layerWidth, 0)).join('')}
    </svg>
    `;
  }
  compile(name = 'AI') {
    let sanitizedName = name.replace(/[^A-Za-z0-9]/g, ''); // there are more characters than this that are valid in JS but I cba to look up which ones and make a regex that sorts it all out. So english alpha-numeric will have to suffice for now.
    if (sanitizedName.length === 0) {
      // no valid chars left? bad consumer!
      sanitizedName = 'AI';
    }
    // this is the only untrusted data used in the entire thing.
    // I did look into using AST to generate the code but it looked a nightmare, and probably total overkill.
    // if it turns out there is other untrusted data then I can revist this decision later, but for now string concatenation will do the trick just fine.
    let previousLayer = this.inputs;
    return `
class ${sanitizedName} {
  async saveScore(avgScore) {
    await fetch({url:\`/api/AI/${sanitizedName}/score\`, method:'post', data: avgScore});
  }
  processFrame(inputs) {
    // process a single frame of inputs and returns the corrosponding outputs
    ${this.layers.map((layer, i) => {
    const result = layer.compile(`layer${i}`, previousLayer).replace(/\n/g, '\n    ');
    previousLayer = layer;
    return result;
  }).join('')}
    ${this.outputs.compile(`processOutputs`, previousLayer).replace(/\n/g, '\n    ')}
  
    this.inputs = inputs;
    let values = inputs;
    this.layervalues = [];
  ${this.layers.map((l, i) => {
    const gen = `
    values = layer${i}(values);
    this.layervalues.push(values);`;
    return gen;
  }).join('')}
    this.outputs = processOutputs(values);
    return this.outputs;
  }
}
export { ${sanitizedName} as default };
`;
  }
}

export { NeuralNet as default };
