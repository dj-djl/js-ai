import Layer from './layer';

class FixedLayer extends Layer {
  constructor(numInputs) {
    super(numInputs, {
      die: 0,
      splitIdentity: 0,
      splitRandom: 0,
    });
  }
}
export default FixedLayer;
