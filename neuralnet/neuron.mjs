import { MutableFloat, defaultMutability } from './mutableValues';
import ClampingFunction from './clamping';
import { getLogRandom, getColour2, getColour } from './util';
import getId from './objectId';


class Neuron {
  constructor() {
    this.bias = new MutableFloat(getLogRandom()); // ideally this should be between -numLinks and numLinks, because if its outside that range then the incoming data can't possibly overwhelm the bias. But I'm too lazy to code this in, instead lets just assume that the evolution will take care of it.
    this.cloneChance = new MutableFloat(0, defaultMutability); // having 2 ways to split and only 1 to die, but all with the same chance
    this.randomChance = new MutableFloat(0, defaultMutability); // means that splitting is more likely than dieing. But this is ok, because
    this.dieChance = new MutableFloat(0, defaultMutability); // nets evolving to become more complex is probably what we actually want...
    this.clampingFunction = new ClampingFunction();
    this.links = [];
    this.newLinkChance = new MutableFloat(0, defaultMutability);
  }
  drawToSVG(centre, radius, previousLayerNodePositions, previousLayerOffsetX, previousLayerOffsetY) {
    const linksSVG = this.links.map((link) => {
      const linkedNode = previousLayerNodePositions.get(link.srcNeuron);
      return `
    <path
    style="fill:none;fill-rule:evenodd;stroke:${getColour(link.weight.value)};stroke-width:${(Math.abs(link.weight.value * 1.25) + 0.25)}px;stroke-linecap:butt;stroke-linejoin:miter;stroke-opacity:1"
    d="M ${centre.x},${centre.y} ${linkedNode.x + previousLayerOffsetX},${linkedNode.y + previousLayerOffsetY}"
    />
`;
    }).join('');

    return `  
    <g>
      <circle
        style="fill: none; stroke:${getColour2(this.bias.value)};stroke-width:${2 + (Math.log(Math.log(this.bias.value)) / 7)};"
        cx="${centre.x}"
        cy="${centre.y}"
        r="${radius}"
      />
      ${this.clampingFunction.drawToSVG(centre.x, centre.y, radius * 0.8)}
    ${linksSVG}
    </g>`;
  }
  compile(previousLayer, namePrefix) {
    const incomingLinks = this.links.map(l => `(previousLayerValues[${previousLayer.neurons.indexOf(l.srcNeuron)}] * ${l.weight.value})
    `);
    return `${this.clampingFunction.compile(`${namePrefix}${this.name}Clamp`)}
function ${namePrefix}${this.name}(previousLayerValues) {
  const unclampedValue = ${[...incomingLinks, this.bias.value].join(`+ `)};
  // console.log('${namePrefix}${this.name}', previousLayerValues, unclampedValue, ${namePrefix}${this.name}Clamp(unclampedValue), ${namePrefix}${this.name}Clamp.toString(), ${namePrefix}${this.name}Clamp);
  return ${namePrefix}${this.name}Clamp(unclampedValue);
}`;
  }
  get name() {
    return `neuron${this.id}`;
  }
  get id() {
    return getId(this);
  }
}
export default Neuron;
