import Neuron from './neuron';

class Layer {
  constructor(numNeurons, probabilities) {
    this.probabilities = probabilities;
    this.neurons = new Array(numNeurons).fill(0).map(() => new Neuron());
  }
  drawToSVG(previousLayerNodePositions, width, height, xOffset, yOffset) {
    const nodeHeightAllocation = height / this.neurons.length;
    const neuronsSVG = this.neurons.map((neuron, num) => {
      const centre = {
        x: width / 2,
        y: (nodeHeightAllocation * num) + (nodeHeightAllocation / 2),
      };
      return neuron.drawToSVG(centre,
        Math.min(nodeHeightAllocation / 3, width / 5),
        previousLayerNodePositions,
        -width,
        0);
    }).join('');

    const svg = `
  <g transform="translate(${xOffset},${yOffset})">
  ${neuronsSVG}
  </g>`;
    previousLayerNodePositions.clear();
    this.neurons.forEach((node, num) => {
      previousLayerNodePositions.set(node, {
        x: width / 2,
        y: (nodeHeightAllocation * num) + (nodeHeightAllocation / 2),
      });
    });
    return svg;
  }

  compile(functionName, previousLayer) {
    return `${this.neurons.map(neuron => neuron.compile(previousLayer, functionName)).join('')}
function ${functionName}(previousLayerValues) {
  return [
    ${this.neurons.map(neuron => `${functionName}${neuron.name}(previousLayerValues)`).join(',\n    ')},
  ];
}`;
  }
}
export default Layer;
