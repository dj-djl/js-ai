import Layer from './layer';
import { getRandom } from './util';
import { MutableFloat, defaultMutability } from './mutableValues';


class VariableLayer extends Layer {
  constructor(numNeurons) {
    // but actually, you know what? lets add a tiny bit of randomness here
    // by using the expanded range we have about a 4.5% chance for the rounded number to be +1 or -1
    const numNeuronsAdjusted = numNeurons + parseInt(getRandom(-1.1, 1.1), 10);
    // console.log(numNeurons, numNeuronsAdjusted);
    super(numNeuronsAdjusted, {
      die: new MutableFloat(0, defaultMutability), // having 2 ways to split and only 1 to die, but all with the same chance
      splitIdentity: new MutableFloat(0, defaultMutability), // means that splitting is more likely than dieing. But this is ok, because
      splitRandom: new MutableFloat(0, defaultMutability), // nets evolving to become more complex is probably what we actually want...
    });
  }
}
export default VariableLayer;
