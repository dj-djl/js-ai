import { MutableEnum, MutableFloat } from './mutableValues';

const rgxClampingFunctionPrefix = /\n {4}/g; // all multi-line clamping functions start with 6 spaces at time of writing. Adjust if necassary (only really affects linting, so not super critical)
const rgxfinalIndent = / *}$/;
const ClampingFunctions = {
  // https://en.wikipedia.org/wiki/Logistic_function
  Sigmoid: {
    svgText: 'S',
    name: 'Sigmoid',
    params: [{
      name: 'base',
      min: Number.MIN_VALUE,
      max: Math.log(Number.MAX_VALUE),
      type: 'log',
    }],
    func: (val, base) => 1 / (1 + (base ** -val)),
  },
  // min & max are for the input range, the output range is always 0-1
  Truncate: {
    svgText: 'T',
    name: 'Truncate',
    params: [
      {
        name: 'min',
        min: Number.MIN_VALUE / 2,
        max: Number.MAX_VALUE / 2,
        type: 'log',
      },
      {
        name: 'max',
        min: Number.MIN_VALUE / 2,
        max: Number.MAX_VALUE / 2,
        type: 'log',
      },
    ],
    func: (val, min, max) => {
      // console.log('truncate', val, min, max);
      let ret;
      let minToUse = min;
      let maxToUse = max;
      if (min === max) {
        // While unlikely, this can happen because the min & max values are totally random
        // we want to discourage such things. so I think here we should just return 0 always;
        // it's highly likely that this will cause bad performance and hence be evolved away
        return 0;
      } else if (minToUse > maxToUse) {
        // this is also possible for the same reason, but we can fix it.
        [minToUse, maxToUse] = [max, min];
      }
      if (val < minToUse) {
        ret = minToUse;
      } else if (val > maxToUse) {
        ret = maxToUse;
      } else {
        ret = val;
      }
      return (ret - minToUse) / (maxToUse - minToUse);
    },
  },
  ArcTan: {
    svgText: 'at',
    name: 'ArcTan',
    params: [],
    func: val => Math.atan(val) / (Math.PI / 2),
  },
// can add more of these later
};


class ClampingFunction extends MutableEnum {
  compile(funcName = this.name) {
    const origFunctionReformatted = this.value.func.toString()
      .replace(rgxClampingFunctionPrefix, '\n    ')
      .replace(rgxfinalIndent, '  }');
    if (this.params.length > 0) {
      // console.log(this.params);
      const paramsJSONescaped = JSON.stringify(this.params.map(p => p.value.valueOf()), null, 2).replace(/`/g, '\\`');
      return `
const ${funcName}FixedParams = JSON.parse(\`${paramsJSONescaped}\`);
const ${funcName}Orig = ${this.value.func.toString().replace(rgxClampingFunctionPrefix, '\n').replace(/ *}$/, '}')};
function ${funcName}(val2) {
  return ${funcName}Orig(val2, ...${funcName}FixedParams);
}`;
    } else {
      // use a more streamlined version
      return `
function ${funcName}(val2) {
  return (${origFunctionReformatted})(val2);
}`;
    }
  }
  get name() {
    return `clampingFunction${this.id.toString(16)}`;
  }
  get id() {
    return getId(this);
  }
  constructor() {
    super(ClampingFunctions);
    this.params = this.value.params.map(p => Object.assign({}, p));
    this.params.forEach((p) => {
      p.value = new MutableFloat(p.min, p.max);
    });
  }
  drawToSVG(cx, cy, size) {
    return `<text x="${cx}" y="${cy}" style="text-align: center; text-anchor: middle; alignment-baseline: middle; font-size: ${size}px">${this.value.svgText}</text>`;
  }
  clamp(val) {
    return this.value.func(val, ...this.params.map(p => p.value));
  }
// don't use this, it uses eval. Instead, write all the clamping functions more globally and just reference the right right, passing it the params at call-time
//   getClampFunction() {
//     // eslint-disable-next-line no-new-func
//     return new Function('val',ClampingFunctions `
// const f = ${this.value.func};
// return f(val, ...JSON.parse("${JSON.stringify(this.params.map(p => p.value)).replace(/"/g, '\\"')}"));
// `);
//   }
}

export default ClampingFunction;
