let nextId = 0;
const objectIdMap = new WeakMap();

export default function getId(obj) {
  if (objectIdMap.has(obj)) {
    return objectIdMap.get(obj);
  } else {
    nextId += 1;
    const newId = nextId.toString(16);
    objectIdMap.set(obj, newId);
    return newId;
  }
}
