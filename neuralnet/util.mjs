import Colour from 'color';

function getRandom(min, max) {
  return (Math.random() * (max - min)) + min;
}
const logBase = Math.E;
const maxLogValue = Math.log(Math.log(Number.MAX_VALUE)) * 1E-10;
function getLogRandom() {
  // a logarthimic random number is unbounded in the positive direction.
  // i.e. it can theoretically have any value
  // but it is more likely to be low
  // I don't think it much matters which base we use here.
  // but exponenting a single time, I didn't like the curve - too many sky high numbers
  // so lets exponent it twice!
  // tbh I still don't much like the curve, but I don't seem to be able to do much about it, so I'll guess we'll have to hope that with a large enough seed population the bad selections will simply die out.
  const a = Math.random() * maxLogValue;
  return logBase ** logBase ** a;
}

function getColour(weight) {
  if (weight > 0) {
    return Colour('#0000FF').mix(Colour('#00FF00'), weight).toString();
  } else {
    return Colour('#FF0000').mix(Colour('#0000FF'), weight + 1).toString();
  }
}
function getColour2(bias) {
  return Colour('#808080').mix(Colour('#000000'), Math.log(Math.log(bias)) / 10).toString();
}

//  function bindFunc(func, ...bindParams) {
//    let bindParamsJSON = JSON.stringify(bindParams).replace(/"/g, '\"');
//    let tl = `
//  const f = ${func.toString()};
//  const bindParams = JSON.parse("${bindParamsJSON}");
//  return (...params)=>f(...bindParams, ...params);
//    `;
//    return new Function(tl);
//  }

export {
  getRandom,
  getLogRandom,
  getColour,
  getColour2,
};

// const nums = [];
// for (let i = 0;i < 10000; i+=1)
// {
//   nums.push(getLogRandom());
// }
// console.log((nums));
