// import Neuralnet from '../neuralnet/neuralnet';
import Generation from '../neuralnet/generation';
import API from './API';

const currentPopulation = new Generation(1024, 0, 'Digits');

// console.log([...currentPopulation.keys()]);

async function get(queryParams) {
  if (!queryParams || !queryParams.has('item')) {
    return JSON.stringify([...currentPopulation.keys()]);
  }
  const item = queryParams.get('item');
  // console.log('AI get', item);
  const ai = currentPopulation.get(item);
  // console.log(ai);
  return ai.compile(item);
}
async function post(action, ...args) {
  switch (action) {
    case 'breed':
      currentPopulation.breed(args[0]);
      return currentPopulation.length;
    default:
      throw new Error(`Unknown action ${action} for AI`);
  }
}
export { get, post };
export const path = 'AI';
export const responseType = 'text';
export const contentType = 'application/javascript';


export default class AI extends API {
  constructor() {
    super({
      path: 'AI',
      verb: 'GET',
      func: () => [...currentPopulation.keys()],
      responseType: 'json',
      contentType: 'application/javascript',
    }, {
      path: 'AI/{name}/js',
      verb: 'GET',
      func: name => currentPopulation.get(name).compile(),
      responseType: 'text',
      contentType: 'application/javascript',
      paramsOrder: ['name'],
    }, {
      path: 'AI/{name}/action/breed?size={newPopulationSize}',
      verb: 'PUT',
      func: (newPopulationSize = 1024) => {
        currentPopulation.breed(newPopulationSize);
        return currentPopulation.length;
      },
      responseType: 'text',
      contentType: 'application/javascript',
      paramsOrder: ['newPopulationSize'],
    });
  }
}
