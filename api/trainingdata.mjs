import API from './API';
import ByteStream from './bytestream';

const loadData = new Promise(async (resolve, reject) => {
  try {
    const data = new ByteStream('training data/train-images-idx3-ubyte', false);
    const labels = new ByteStream('training data/train-labels-idx1-ubyte', false);
    const dataMagic = await data.readUInt32();
    const labelMagic = await labels.readUInt32();
    if (dataMagic !== 2051) {
      throw new Error(`Data file in wrong format: ${dataMagic}`);
    }
    if (labelMagic !== 2049) {
      throw new Error(`Label file in wrong format: ${labelMagic}`);
    }
    let numItems = await data.readUInt32();

    if (await labels.readUInt32() !== numItems) {
      throw new Error('data and labels file disagree on item count');
    }
    numItems = 1000; // override for quicker loading

    const height = await data.readUInt32();
    const width = await data.readUInt32();

    const outImages = [];
    const outLabels = [];
    for (let item = 0; item < numItems; item += 1) {
      if (item % 1000 === 0) {
        console.log(`${item} / ${numItems}`);
      }
      const imgData = [];
      for (let y = 0; y < height; y += 1) {
        const row = [];
        for (let x = 0; x < width; x += 1) {
          row.push(await data.readByte()); // eslint-disable-line no-await-in-loop
        }
        imgData.push(row);
      }
      outImages.push(imgData);
      outLabels.push(await labels.readByte()); // eslint-disable-line no-await-in-loop
    }
    resolve({
      images: outImages,
      labels: outLabels,
      numItems,
    });
  } catch (ex) {
    reject(ex);
  }
});

loadData.then((data) => {
  console.log(`${data.numItems} training images loaded`);
});


export default class TrainingData extends API {
  constructor() {
    super({
      path: 'trainingdata',
      verb: 'GET',
      func: async function get() {
        const data = await loadData;
        const chosenItem = parseInt(Math.random() * data.numItems, 10);

        return {
          label: data.labels[chosenItem],
          imgData: data.images[chosenItem],
          chosenItem,
        };
      },
      responseType: 'json',
    });
  }
}
