import fs from 'mz/fs';

// warning this class has async methods that are not threadsafe!
// use with care!
// TODO: make it threadsafe!


class ByteStream {
  constructor(src, littleEndian) {
    if (typeof (src) === 'string') {
      this.src = fs.createReadStream(src);
    } else {
      this.src = src;
    }
    this.littleEndian = littleEndian;
    this.bufferReady = new Promise((resolve, reject) => {
      this.buffer = Buffer.alloc(10240000);
      let offset = 0;

      this.src.on('error', reject);
      this.src.on('end', () => {
        const buff = new ArrayBuffer(this.buffer.length);
        const buff2 = Buffer.from(buff);
        this.buffer.copy(buff2, 0, 0);
        this.buffer = new DataView(buff);

        this.offset = 0;
        resolve();
      });

      this.src.on('data', (buff) => {
        if (this.buffer.length < offset + buff.length) {
          this.extendBuffer();
        }
        offset += buff.copy(this.buffer, offset, 0);
      });
    });
  }
  extendBuffer() {
    const oldBuffer = this.buffer;
    // console.log('extending buffer to ', oldBuffer.length * 2);
    this.buffer = Buffer.alloc(oldBuffer.length * 2);
    oldBuffer.copy(this.buffer, 0, 0);
  }
  async readByte() {
    await this.bufferReady;
    const result = this.buffer.getUint8(this.offset, this.littleEndian);
    this.offset += 1;
    return result;
  }
  async readInt16() {
    await this.bufferReady;
    const result = this.buffer.getInt16(this.offset, this.littleEndian);
    this.offset += 2;
    return result;
  }
  async readUInt16() {
    await this.bufferReady;
    const result = this.buffer.getUint16(this.offset, this.littleEndian);
    this.offset += 2;
    return result;
  }
  async readUInt32() {
    await this.bufferReady;
    // console.log(this.buffer, this.littleEndian, this.offset);
    const result = this.buffer.getUint32(this.offset, this.littleEndian);
    // console.log('>', result);
    this.offset += 4;
    return result;
  }
  async readInt32() {
    await this.bufferReady;
    const result = this.buffer.getInt32(this.offset, this.littleEndian);
    this.offset += 4;
    return result;
  }
  seek(offset) {
    this.offset = offset;
  }
}

export { ByteStream as default };
