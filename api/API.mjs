const rgxParam = /^\{([^}]*)\}$/;

class Method {
  constructor(method) {
    this.path = method.path;
    // this.pathParts = method.path.split(/\//);
    this.pathURL = method.path.split(/\//);
    this.verb = method.verb.toLowerCase();
    this.func = method.func;
    this.responseType = method.responseType;
    this.contentType = method.contentType;
  // this.regex = '';
  // let plainText = '';
  // let state = 0;
  // Array.from(method.path)
  //   .forEach((ch, idx) => {
  //     if (state === 0 && ch === '\\') {
  //       state = 1;
  //     } else if (state === 1) {
  //       plainText += ch;
  //       state = 0;
  //     } else if (ch === '{') {
  //       state = 2;
  //       this.regex += RegExp.escape(plainText);
  //       plainText = '';
  //     } else if (state === 2 && ch === '}') {
  //         this.regex += '([^/]*)'
  //       state = 0;
  //     } else if (state !== 2 && ch === '}') {
  //       throw new Error(`mismatched } at ${idx}`);
  //     } else if (state === 0) {
  //       plainText += ch;
  //     } else {
  //       throw new Error('Invalid state?');
  //     }
  //   });
  // if (state === 1) {
  //   throw new Error('Mismatched \\');
  // } else if (state === 2) {
  //   throw new Error('Mismatched {');
  // }
  // new RegExp(method.path.replace(/\{([^}]*)\}/g, ``))
  }
  test(url) {
    if (url.length !== this.pathURL.length) {
      // get out early
      return undefined;
    }
    const namedParams = {};
    let noMatch = false;
    url.forEach((left, i) => {
      if (noMatch) {
        return;
      }
      const right = this.pathURL[i];
      const param = rgxParam.exec(right);
      if (param) {
        namedParams[param[1]] = left;
      } else if (left !== right) {
        noMatch = true;
      }
    });
    let run;

    if (noMatch) {
      return false;
    } else if (this.paramOrder) {
      const paramsSorted = this.paramOrder.map(name => namedParams[name]);
      run = () => {
        this.func(...paramsSorted);
      };
    } else if (this.namedParams) {
      run = () => {
        this.func(namedParams);
      };
    } else if (Object.keys(namedParams).length === 1) {
      // with a single param, we can just pass it as the only param
      const param = namedParams[Object.keys(namedParams)];
      run = () => this.func(param);
    } else if (Object.keys(namedParams).length === 0) {
      // no params? no problem!
      run = () => this.func();
    }
    return Object.assign({}, this, {
      run,
    });
  }
}

const sMethods = Symbol('Methods');

export default class API {
  constructor(...methods) {
    this[sMethods] = [];
    methods.forEach(m => this.registerMethod(m));
  }
  registerMethod(method) {
    this[sMethods].push(new Method(method));
  }
  find(url, verb) {
    const verbLC = verb.toLowerCase();
    for (const method of this[sMethods]) {
      if (method.verb === verbLC) {
        const out = method.test(url);
        if (out) {
          return out;
        }
      }
    }
    return undefined;
  }
}
